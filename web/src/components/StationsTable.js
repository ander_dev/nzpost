import React, {Component} from "react";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

class StationsTable extends Component {
    render() {
        const {classes} = this.props;
        let rows = [];
        this.props.stations.forEach((station) => {
            rows.push(<StationRow station={station}/>);
        });
        return (
            <div>
                <Typography variant="h6" id="tableTitle">
                    List of Stations
                </Typography>
                <Table className={classes.table} size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">avg Transit Time</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

class StationRow extends React.Component {
    render() {
        const {station} = this.props;
        return (
            <TableRow key={'table_' + station.id}>
                <TableCell component="th" scope="row">
                    {station.name}
                </TableCell>
                <TableCell align="right">{station.avgTransitTime}</TableCell>
            </TableRow>
        );
    }
}

export default StationsTable;