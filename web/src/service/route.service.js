import axios from 'axios'

export const routeService = {
    getBestRoute,
    getStations
}

let axiosConfig = {
    baseURL: 'http://localhost:8080/api',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    }
}

function getBestRoute(from, to) {
    return axios.get('/route/' + from + '/' + to, axiosConfig)
        .then(function (response) {
            console.log(response.data)
            return response.data
        })
        .catch(function (error) {
            console.log(error.response)
            return error.response
        })
}

function getStations() {
    return axios.get('/stations', axiosConfig)
        .then(function (response) {
            console.log(response.data)
            return response.data
        })
        .catch(function (error) {
            console.log(error.response)
            return error.response
        })
}