import React, {Component} from 'react';
import {routeService} from "./service/route.service"
import "./App.css";
import 'typeface-roboto';
import LinearBuffer from "./components/LinearBuffer";
import StationsTable from "./components/StationsTable";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    where_to: {
        ...theme.typography.button,
        fontSize: 18,
        padding: theme.spacing(1),
    },
    root: {
        background: 'linear-gradient(45deg, #4c8cd8 30%, #27AAE1 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
    searchingBox: {
        maxWidth: '600px',
        minWidth: '400px',
        minHeight: '90px',
        position: 'absolute',
        top: '5%',
        left: 0,
        right: 0,
        margin: 'auto',
        textAlign: 'center',
        borderRadius: '25px',
        border: '0px solid blue',
    },
    stationsBox: {
        maxWidth: '800px',
        minHeight: '90px',
        position: 'absolute',
        top: '45%',
        left: 0,
        right: 0,
        margin: 'auto',
        textAlign: 'center',
        borderRadius: '25px',
        border: '0px solid blue',
    },
    responseBox: {
        maxWidth: '800px',
        minHeight: '90px',
        marginTop: '50px',
        textAlign: 'center',
        borderRadius: '25px',
        border: '0px solid blue',
        ...theme.typography.button,
        fontSize: 18,
        color: 'blue',
        padding: theme.spacing(1),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
});

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            stations: [],
            message: "",
            display: false,
            fromStation: 1,
            toStation: 0,
            selectedOption: null,
        }

        this.handleChangeFrom = this.handleChangeFrom.bind(this);
        this.handleChangeTo = this.handleChangeTo.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        routeService.getStations().then(
            data => {
                if (data !== undefined) {
                    this.setState({
                        stations: data.objReturn
                    })
                }
            },
            error => {
                console.log(error)
            }
        );
    }

    handleChangeFrom(event) {
        this.setState({fromStation: event.target.value});
    };

    handleChangeTo(event) {
        this.setState({
            toStation: event.target.value
        });
        this.handleSubmit(event.target.value);
    };

    handleSubmit(toStation) {
        this.setState({
            display: true
        });
        routeService.getBestRoute(this.state.fromStation, toStation).then(
            data => {
                console.log(data)
                this.setState({
                    message: data.objReturn
                })
                this.setState({
                    display: false
                })
            },
            error => {
                console.log(error)
                this.setState({
                    message: "Oooopsy, something wrong is not right, can you please try again later, thanks."
                })
            }
        );
    }

    render() {
        const {classes} = this.props;

        let stations = this.state.stations;

        let rows = [];
        stations.forEach((station) => {
            rows.push(<MenuItemRow station={station}/>);
        });

        return (
            <div className="bgImage">

                <div className={classes.searchingBox}>

                    <div className={classes.where_to}>
                        Please inform where to?
                    </div>

                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="from-simple">From</InputLabel>
                        <Select
                            value={this.state.fromStation}
                            onChange={this.handleChangeFrom}
                            inputProps={{
                                name: 'from',
                                id: 'from-simple',
                            }}
                        >
                            <MenuItem value="" disabled>
                                <em>Select Station</em>
                            </MenuItem>

                            {stations.map((item, key) =>
                                <MenuItem key={key} value={item.id}> {item.name} </MenuItem>
                            )}
                        </Select>
                    </FormControl>

                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="from-simple">To</InputLabel>
                        <Select
                            value={this.state.toStation}
                            onChange={this.handleChangeTo}
                            inputProps={{
                                name: 'to',
                                id: 'to-simple',
                            }}
                        >
                            <MenuItem value="" disabled>
                                <em>Select Station</em>
                            </MenuItem>

                            {stations.map((item, key) =>
                                <MenuItem key={key} value={item.id}> {item.name} </MenuItem>
                            )}
                        </Select>
                    </FormControl>

                    {this.state.display &&
                    <div className={classes.responseBox}><LinearBuffer className={classes}/></div>}
                    {!this.state.display && <div className={classes.responseBox}> {this.state.message} </div>}

                </div>

                <div className={classes.stationsBox}>
                    <StationsTable stations={this.state.stations} classes={classes}/>
                </div>
            </div>
        );
    }
}

class MenuItemRow extends Component {
    render() {
        const {station} = this.props;
        return (
            <MenuItem value={station.id}> {station.name} </MenuItem>
        );
    }
}

export default withStyles(styles)(App);
