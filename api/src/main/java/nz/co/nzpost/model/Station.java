package nz.co.nzpost.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "station")
@EntityListeners(AuditingEntityListener.class)
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer avgTransitTime;

    public Station() {
    }

    public Station(String name, int avgTransitTime) {
        this.name = name;
        this.avgTransitTime = avgTransitTime;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAvgTransitTime() {
        return avgTransitTime;
    }
}
