package nz.co.nzpost.model.dto;

public class ResponseDTO {

    private int statusCode;
    private Object objReturn;

    //empty constructor is needed by Jackson because a second constructor has been added.
    public ResponseDTO() {
    }

    public ResponseDTO(int statusCode, Object objReturn) {
        this.statusCode = statusCode;
        this.objReturn = objReturn;
    }

    @Override
    public String toString() {
        return String.format("ResponseDTO { status = %s, message = %s }", statusCode, objReturn);
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Object getObjReturn() {
        return objReturn;
    }
}

