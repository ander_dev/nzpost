package nz.co.nzpost.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "connection")
@EntityListeners(AuditingEntityListener.class)
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Station.class)
    @JoinColumn(name = "from_station_id", foreignKey = @ForeignKey(name = "FK_CONNECTION_STATION_FROM"))
    private Station fromStation;

    @ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Station.class)
    @JoinColumn(name = "to_station_id", foreignKey = @ForeignKey(name = "FK_CONNECTION_STATION_TO"))
    private Station toStation;

    private Integer duration;

    public Connection() {
    }

    public Connection(Station fromStation, Station toStation, Integer duration) {
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public Station getFromStation() {
        return fromStation;
    }

    public Station getToStation() {
        return toStation;
    }

    public Integer getDuration() {
        return duration;
    }
}
