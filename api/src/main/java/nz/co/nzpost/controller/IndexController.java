package nz.co.nzpost.controller;

import nz.co.nzpost.model.dto.ResponseDTO;
import nz.co.nzpost.service.ConnectionService;
import nz.co.nzpost.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class IndexController {

    @Autowired
    private StationService stationService;

    @Autowired
    private ConnectionService connectionService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseDTO status() {
        return new ResponseDTO(200, "Application up and running!");
    }

    @RequestMapping(value = "/stations", method = RequestMethod.GET, produces = "application/json")
    public ResponseDTO listStations() {
        return stationService.list();
    }

    @RequestMapping(value = "/route/{idFrom}/{idTo}", method = RequestMethod.GET, produces = "application/json")
    public ResponseDTO bestRoute(@PathVariable(value = "idFrom") Long idFrom, @PathVariable(value = "idTo") Long idTo) {
        return connectionService.routeMe(idFrom, idTo);
    }
}
