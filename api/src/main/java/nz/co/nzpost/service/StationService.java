package nz.co.nzpost.service;

import nz.co.nzpost.model.Station;
import nz.co.nzpost.model.dto.ResponseDTO;

import java.util.List;

public interface StationService {

    ResponseDTO saveBulk(List<Station> stations);

    ResponseDTO list();

}