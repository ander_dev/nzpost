package nz.co.nzpost.service;

import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.dto.ResponseDTO;

import java.util.List;

public interface ConnectionService {

    ResponseDTO saveBulk(List<Connection> connections);

    ResponseDTO list();

    ResponseDTO routeMe(Long ifFrom, Long idTo);

}