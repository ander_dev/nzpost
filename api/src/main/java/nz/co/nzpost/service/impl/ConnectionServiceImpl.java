package nz.co.nzpost.service.impl;

import nz.co.nzpost.config.ServerResponseCode;
import nz.co.nzpost.logback.AppLogger;
import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.Edge;
import nz.co.nzpost.model.Station;
import nz.co.nzpost.model.Vertex;
import nz.co.nzpost.model.dto.ResponseDTO;
import nz.co.nzpost.repository.ConnectionRepository;
import nz.co.nzpost.repository.StationRepository;
import nz.co.nzpost.service.ConnectionService;
import nz.co.nzpost.service.DijkstraAlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConnectionServiceImpl implements ConnectionService {

    private final static AppLogger logger = AppLogger.getInstance();

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private DijkstraAlgorithmService dijkstraAlgorithmService;

    @Override
    @Transactional
    public ResponseDTO saveBulk(List<Connection> connections) {
        try {
            int x = 0;
            for (Connection connection : connections) {
                if (connection.getDuration() == null || connection.getFromStation() == null || connection.getToStation() == null) {
                    return new ResponseDTO(ServerResponseCode.BAD_REQUEST, "One of the connections must have null data, please verify ans resend list.");
                }
                connectionRepository.save(connection);
                x++;
            }
            ResponseDTO responseDTO = new ResponseDTO(ServerResponseCode.SUCCESS, String.format("Successfully, just save %s connections!", x));
            return responseDTO;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return new ResponseDTO(ServerResponseCode.SERVER_ERROR, "Something went wrong!");
        }
    }

    @Override
    public ResponseDTO list() {
        return new ResponseDTO(ServerResponseCode.SUCCESS, connectionRepository.findAll());
    }

    private List<Vertex> nodes;
    private List<Edge> edges;

    @Override
    public ResponseDTO routeMe(Long idFrom, Long idTo) {
        if (idFrom == null || idTo == null) {
            return new ResponseDTO(ServerResponseCode.BAD_REQUEST, "Either from on to must be specified!");
        }

        Station fromStation = stationRepository.findById(idFrom).get();
        Station toStation = stationRepository.findById(idTo).get();

        String ret = String.format("The best route to get from station %s to %s is: ", fromStation.getName(), toStation.getName());
        ret += dijkstraAlgorithmService.getBestPathBetweenNodes(fromStation, toStation);

        return new ResponseDTO(ServerResponseCode.SUCCESS, ret);
    }

    private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
        Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
        edges.add(lane);
    }

    protected String getStation(String station) {
        String ret = null;
        switch (station) {
            case "Node_0": {
                ret = "A";
                break;
            }
            case "Node_1": {
                ret = "B";
                break;
            }
            case "Node_2": {
                ret = "C";
                break;
            }
            case "Node_3": {
                ret = "D";
                break;
            }
            case "Node_4": {
                ret = "E";
                break;
            }
            case "Node_5": {
                ret = "F";
                break;
            }
            case "Node_6": {
                ret = "G";
                break;
            }
            case "Node_7": {
                ret = "H";
                break;
            }
            case "Node_8": {
                ret = "I";
                break;
            }
            case "Node_9": {
                ret = "J";
                break;
            }
        }
        return ret;
    }
}
