package nz.co.nzpost.service.impl;

import io.micrometer.core.instrument.util.StringUtils;
import nz.co.nzpost.config.ServerResponseCode;
import nz.co.nzpost.logback.AppLogger;
import nz.co.nzpost.model.Station;
import nz.co.nzpost.model.dto.ResponseDTO;
import nz.co.nzpost.repository.StationRepository;
import nz.co.nzpost.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StationServiceImpl implements StationService {

    private final static AppLogger logger = AppLogger.getInstance();

    @Autowired
    private StationRepository stationRepository;

    @Override
    @Transactional
    public ResponseDTO saveBulk(List<Station> stations) {
        try {
            int x = 0;
            for (Station station : stations) {
                if (StringUtils.isBlank(station.getName()) || station.getAvgTransitTime() == null) {
                    return new ResponseDTO(ServerResponseCode.BAD_REQUEST, String.format("One of the stations must have name or transit time empty or null, please verify ans resend list."));
                }
                stationRepository.save(station);
                x++;
            }
            ResponseDTO responseDTO = new ResponseDTO(200, String.format("Successfully, just save %s stations!", x));
            return responseDTO;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return new ResponseDTO(400, "Something went wrong!");
        }
    }

    @Override
    public ResponseDTO list() {
        return new ResponseDTO(200, stationRepository.findAll());
    }
}
