package nz.co.nzpost.service.impl;

import nz.co.nzpost.model.*;
import nz.co.nzpost.repository.ConnectionRepository;
import nz.co.nzpost.service.DijkstraAlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DijkstraAlgorithmServiceImpl implements DijkstraAlgorithmService {

    @Autowired
    private ConnectionRepository connectionRepository;

    private List<Vertex> nodes;
    private List<Edge> edges;

    @Override
    public String getBestPathBetweenNodes(Station fromStation, Station toStation) {

        List<Connection> connections = connectionRepository.findAll().stream().sorted(Comparator.comparing(Connection::getId)).collect(Collectors.toList());

        nodes = new ArrayList<>();
        edges = new ArrayList<>();

        for (int i = 0; i < connections.size(); i++) {
            Vertex location = new Vertex("Node_" + i, "Node_" + i);
            nodes.add(location);
        }

        int z = 0;
        for (Connection connection : connections) {
            addLane("Edge_" + z++, connection.getFromStation().getId().intValue() - 1, connection.getToStation().getId().intValue() - 1, connection.getDuration());
        }

        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(nodes.get(fromStation.getId().intValue() - 1));
        LinkedList<Vertex> path = dijkstra.getPath(nodes.get(toStation.getId().intValue() - 1));

        if (path == null) {
            return "You are already here!";
        }

        String ret = "";

        int x = 0;
        for (Vertex vertex : path) {
            ret += x++ == 0 ? getStation(vertex.getName()) : " --> " + getStation(vertex.getName());
        }

        return ret;
    }

    private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
        Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
        edges.add(lane);
    }

    protected String getStation(String station) {
        String ret = null;
        switch (station) {
            case "Node_0": {
                ret = "A";
                break;
            }
            case "Node_1": {
                ret = "B";
                break;
            }
            case "Node_2": {
                ret = "C";
                break;
            }
            case "Node_3": {
                ret = "D";
                break;
            }
            case "Node_4": {
                ret = "E";
                break;
            }
            case "Node_5": {
                ret = "F";
                break;
            }
            case "Node_6": {
                ret = "G";
                break;
            }
            case "Node_7": {
                ret = "H";
                break;
            }
            case "Node_8": {
                ret = "I";
                break;
            }
            case "Node_9": {
                ret = "J";
                break;
            }
        }
        return ret;
    }
}
