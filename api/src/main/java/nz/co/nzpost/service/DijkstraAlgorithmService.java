package nz.co.nzpost.service;

import nz.co.nzpost.model.Station;

public interface DijkstraAlgorithmService {

    String getBestPathBetweenNodes(Station fromStation, Station toStation);
}
