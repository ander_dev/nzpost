package nz.co.nzpost.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import nz.co.nzpost.logback.AppLogger;
import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.Station;
import nz.co.nzpost.model.dto.ResponseDTO;
import nz.co.nzpost.service.ConnectionService;
import nz.co.nzpost.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Configuration
public class PreLoadData {

    //Please note that additions below are only here to be faster as in the real world it would be a liquibase, and for this reason they are not tested only their services
    private final static AppLogger logger = AppLogger.getInstance();

    ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @Autowired
    private StationService stationService;

    @Autowired
    private ConnectionService connectionService;

    @Bean
    public void saveStations() {
        try {
            Station[] stationArray = (Station[]) mapFromJsonFile("stations.json");
            List<Station> stations = Arrays.asList(stationArray);
            ResponseDTO responseDTO = stationService.saveBulk(stations);
            logger.info(responseDTO.toString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Bean
    public void saveConnections() {
        try {
            Connection[] connectionArray = (Connection[]) mapFromJsonFile("connections.json");
            List<Connection> connections = Arrays.asList(connectionArray);
            ResponseDTO responseDTO = connectionService.saveBulk(connections);
            logger.info(responseDTO.toString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private Object mapFromJsonFile(String fileName) throws IOException {

        Path path = FileSystems.getDefault().getPath("api/src/main/resources/data", fileName);

        String[] entity = fileName.split(Pattern.quote("."));

        switch (entity[0]) {
            case "stations":
                return mapper.readValue(path.toFile(), Station[].class);
            case "connections":
                return mapper.readValue(path.toFile(), Connection[].class);
            default:
                return null;
        }
    }
}
