package nz.co.nzpost.repository;

import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {

    List<Connection> findByFromStation(Station from);
}
