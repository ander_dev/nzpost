@SERVICES
Feature: Tests services

  Scenario: Save stations in bulk
    Given a list of objects to be saved is available from "stations.json"
    When list of "stations" are saved
    Then the response statusCode should be 200 with message "Successfully, just save 10 stations!"

  Scenario: Save connections in bulk
    Given a list of objects to be saved is available from "connections.json"
    When list of "connections" are saved
    Then the response statusCode should be 200 with message "Successfully, just save 19 connections!"

  @route
  Scenario: Best route between stations
    Given a list of objects to be saved is available from "stations.json"
    And list of "stations" are saved
    And  a list of objects to be saved is available from "connections.json"
    And list of "connections" are saved
    When I want to travel from station "F" to "G"
    Then response status should be 200 with message "The best route to get from station F to G is: F --> A --> G"
