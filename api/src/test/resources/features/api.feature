@API
Feature: API bucket

  Scenario: Is API running
    Given I try to get status from "http://localhost:8080/api"
    Then response statusCode should be 200 with message "Application up and running!"