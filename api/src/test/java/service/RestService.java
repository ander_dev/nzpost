package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.Station;
import nz.co.nzpost.model.dto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.regex.Pattern;

public abstract class RestService {

    protected ResponseDTO responseDTO;

    ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @Autowired
    private TestRestTemplate testRestTemplate;

    protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {
        return mapper.readValue(json, clazz);
    }

    protected String mapFromObjectToString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Object mapFromJsonFile(String fileName) throws IOException {
        Path path = FileSystems.getDefault().getPath("src/main/resources/data", fileName);

        String[] entity = fileName.split(Pattern.quote("."));

        switch (entity[0]) {
            case "stations":
                return mapper.readValue(new File(path.toString()), Station[].class);
            case "connections":
                return mapper.readValue(new File(path.toString()), Connection[].class);
            default:
                return null;
        }
    }


    protected ResponseDTO executeGet(String url) {
        ResponseEntity<ResponseDTO> response = testRestTemplate.getForEntity(url, ResponseDTO.class);
        return response.getBody();
    }

    protected Long getStationId(String station) {
        Long ret = null;
        switch (station) {
            case "A": {
                ret = 1L;
                break;
            }
            case "B": {
                ret = 2L;
                break;
            }
            case "C": {
                ret = 3L;
                break;
            }
            case "D": {
                ret = 4L;
                break;
            }
            case "E": {
                ret = 5L;
                break;
            }
            case "F": {
                ret = 6L;
                break;
            }
            case "G": {
                ret = 7L;
                break;
            }
            case "H": {
                ret = 8L;
                break;
            }
            case "I": {
                ret = 9L;
                break;
            }
            case "J": {
                ret = 10L;
                break;
            }
        }
        return ret;
    }

}
