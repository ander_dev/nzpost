package units;

import nz.co.nzpost.model.Edge;
import nz.co.nzpost.model.Graph;
import nz.co.nzpost.model.Vertex;
import nz.co.nzpost.service.impl.DijkstraAlgorithm;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestDijkstraAlgorithm {

    private List<Vertex> nodes;
    private List<Edge> edges;

    @Test
    public void testExcute() {
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Vertex location = new Vertex("Node_" + i, "Node_" + i);
            nodes.add(location);
        }

        addLane("Edge_0", 0, 6, 16);
        addLane("Edge_1", 0, 4, 20);
        addLane("Edge_2", 0, 3, 19);
        addLane("Edge_3", 0, 1, 12);
        addLane("Edge_4", 1, 8, 15);
        addLane("Edge_5", 1, 2, 5);
        addLane("Edge_6", 1, 3, 13);
        addLane("Edge_7", 2, 3, 5);
        addLane("Edge_8", 3, 4, 7);
        addLane("Edge_9", 4, 5, 5);
        addLane("Edge_10", 5, 0, 5);
        addLane("Edge_11", 6, 5, 11);
        addLane("Edge_12", 7, 6, 6);
        addLane("Edge_13", 7, 0, 4);
        addLane("Edge_14", 7, 1, 19);
        addLane("Edge_15", 8, 7, 21);
        addLane("Edge_16", 8, 9, 10);
        addLane("Edge_17", 9, 1, 7);
        addLane("Edge_18", 9, 2, 15);

        // Lets check from location Loc_1 to Loc_10
        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(nodes.get(1));
        LinkedList<Vertex> path = dijkstra.getPath(nodes.get(6));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

    }

    private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
        Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
        edges.add(lane);
    }
}
