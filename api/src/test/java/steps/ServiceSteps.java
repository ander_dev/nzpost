package steps;

import cucumber.api.java8.En;
import nz.co.nzpost.model.Connection;
import nz.co.nzpost.model.Station;
import nz.co.nzpost.service.ConnectionService;
import nz.co.nzpost.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import service.RestService;

import java.util.Arrays;
import java.util.List;

public class ServiceSteps extends RestService implements En {

    private List<Station> stations;
    private List<Connection> connections;

    @Autowired
    private StationService stationService;

    @Autowired
    private ConnectionService connectionService;

    public ServiceSteps() {

        Given("a list of objects to be saved is available from {string}", (String json) -> {

            switch (json) {
                case "stations.json":
                    Station[] stationArray = (Station[]) mapFromJsonFile(json);
                    stations = Arrays.asList(stationArray);
                    break;
                case "connections.json":
                    Connection[] connectionArray = (Connection[]) mapFromJsonFile(json);
                    connections = Arrays.asList(connectionArray);
                    break;
            }

        });

        Then("the response statusCode should be {int} with message {string}", (Integer statusCode, String message) -> {
            Assert.assertEquals(statusCode.intValue(), responseDTO.getStatusCode());
            Assert.assertEquals(message, responseDTO.getObjReturn());
        });

        Given("I want to travel from station {string} to {string}", (String from, String to) -> {
            responseDTO = connectionService.routeMe(getStationId(from), getStationId(to));
        });

        Then("response status should be {int} with message {string}", (Integer status, String message) -> {
            Assert.assertEquals(status.intValue(), responseDTO.getStatusCode());
            Assert.assertEquals(message, responseDTO.getObjReturn());
        });

        When("list of {string} are saved", (String type) -> {
            switch (type) {
                case "stations":
                    responseDTO = stationService.saveBulk(stations);
                    break;
                case "connections":
                    responseDTO = connectionService.saveBulk(connections);
                    break;
            }
        });
    }
}
