package steps;

import cucumber.api.java8.En;
import org.testng.Assert;
import service.RestService;

public class ApiSteps extends RestService implements En {

    public ApiSteps() {
        Given("I try to get status from {string}", (String url) -> {
            responseDTO = executeGet(url);
        });

        Then("response statusCode should be {int} with message {string}", (Integer statusCode, String message) -> {
            Assert.assertEquals(statusCode.intValue(), responseDTO.getStatusCode());
            Assert.assertEquals(message, responseDTO.getObjReturn());
        });
    }
}


