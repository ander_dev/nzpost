## Micro service to provide faster train route for user

Very simple API, written using Java 8 with Spring boot, H2 DB (in memory, faster impl), 
Selenium with cucumber and gherkins.

In this API, if I had more time I would:
 * use more generics for business and persistence layer
 * extend the tests for the whole integration not only unit tests.
 * as a micro service, would have a real DB in another container or use a noSQL approach.
 
Please note, I have added simple tests just to implement the main functions, to be faster, what I wanted to achieve here is showing you that I do work with TDD as well.
