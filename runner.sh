#!/bin/sh
echo Starting Deployment!
gradle --stop
cd api
gradle bootRun &
cd ../web
npm install
npm start